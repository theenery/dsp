from tkinter import *
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
from scipy.integrate import quad

import numpy as np


#initialize variables
interval = [-np.pi, np.pi]
step = 0.01
T = np.arange(3 * interval[0], 3 * interval[1], step)
subintervals_limit = 300

#set up window
window = Tk()
window.geometry("500x650")
window.title("Lab1")

#set up plot
fig = Figure(figsize = (5, 5), dpi = 100)
subplot = fig.add_subplot(111)

def reset_view():
    global interval, subplot
    space = 2
    subplot.set_xlim(interval[0] - space, interval[1] + space)
    subplot.set_ylim(interval[0] - space, interval[1] + space)


#make it periodic in range from a to b
def bound(t, a, b):
    return a + (t - a) % (b - a) 

#initial function f(t)
def f_init(t):
    t = bound(t, *interval)
    #function
    if t < 0:
        return -(np.pi + t) / 2
    else:
        return (np.pi - t) / 2

def f_alt(t):
    t = bound(t, *interval)
    #alt function
    my_n = 8
    return t**my_n * np.exp(-t * t / my_n)

f = f_init

#Fourier coefficient An in range from a to b
def An(n, a, b):
    return quad(lambda x: (f(x) * np.cos(n * x)), a, b, limit = subintervals_limit)[0] / np.pi

#Fourier coefficient Bn in range from a to b
def Bn(n, a, b):
    return quad(lambda x: (f(x) * np.sin(n * x)), a, b, limit = subintervals_limit)[0] / np.pi

#Fourier series function F(x) with N members
def Furry(x, N):
    global A, B
    sum = A[0] / 2.0
    for n in range(1, N):
        sum += A[n] * np.cos(n * x) + B[n] * np.sin(n * x)
    return sum


def plot(N):
    #calculate An and Bn once
    global A, B
    A = [An(n, *interval) for n in range(0, N)]
    B = [Bn(n, *interval) for n in range(0, N)]

    #calculate mean errors
    abs_error = 0.0
    rel_error = 0.0
    sqr_error1 = 0.0
    epsilon = 1e-6 #prevents division by zero
    for t in T:
        diff = abs(Furry(t, N) - f(t))
        abs_error += diff
        rel_error += diff / (abs(f(t)) + epsilon)
        sqr_error1 += diff * diff
    abs_error /= len(T)
    rel_error /= len(T)
    sqr_error1 /= len(T)

    #square mean error using formula
    temp = 0.0
    for i in range(1, len(A)):
        temp += A[i] * A[i] + B[i] * B[i]
    sqr_error2 = quad(lambda x: f(x) * f(x), *interval)[0] / 2 / np.pi - A[0] * A[0] / 4 - temp / 2

    error_label.config(text = f"\
    Absolute error = {abs_error:.6f}\n\
    Relative error = {rel_error:.6f}\n\
    Squared error using iterations = {sqr_error1:.6f}\n\
    Squared error using formula = {sqr_error2:.6f}")

    #write coeffitients to file
    with open("Lab1/koefs.txt", "+wt") as file:
        file.write(f"N = {N}\n")

        file.write("\n")
        file.write(f"\
        Absolute error = {abs_error:.6f}\n\
        Relative error = {rel_error:.6f}\n\
        Squared error using iterations = {sqr_error1:.6f}\n\
        Squared error using formula = {sqr_error2:.6f}\n")

        file.write("\n")
        for n in range(N):
            file.write(f"a{n} = {A[n]}\n")
        
        file.write("\n")
        for n in range(N):
            file.write(f"b{n} = {B[n]}\n")

    #plot f(x) and F(x)
    vf = np.vectorize(f)
    vF = lambda x: Furry(x, N)
    subplot.clear()
    reset_view()
    subplot.plot(T, vf(T), label='f(x)')
    subplot.plot(T, vF(T), label='Fourier series')
    subplot.legend()
    canvas.draw()


#validate entered n to be greater than 1 integer
def validate(inp):
    try:
        if not inp or int(inp) == 1:
            button.config(state = DISABLED)
            return True
        inp = int(inp)
        if inp > 1:
            button.config(state = NORMAL)
            return True
        else:
            button.config(state = DISABLED)
            return False
    except ValueError:
        return False


#set up ui
vcmd = window.register(validate)
header = Frame(window)
Label(header, text = "N = ").pack(side = LEFT)
entry = Entry(header, 
              width = 5, 
              validate = 'key', 
              validatecommand = (vcmd, '%P'))
entry.pack(side = LEFT)
button = Button(header, 
                text = "apply", 
                command = lambda: plot(int(entry.get())), 
                state = DISABLED)
button.pack(side = LEFT)
header.pack(expand = 1)
error_label = Label(window, text = f"")
error_label.pack()

#draw canvas
canvas = FigureCanvasTkAgg(fig, master = window)
canvas.draw()
canvas.get_tk_widget().pack()

#add toolbar
toolbar = NavigationToolbar2Tk(canvas, window)
toolbar.update()
canvas.get_tk_widget().pack()


window.mainloop()