from tkinter import *
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)

import numpy as np

#initialize variables
interval = [-np.pi, np.pi]
T = np.arange(interval[0] * 2, interval[1] * 2, 0.1)
Y = [2.84, 2.94, 3.20, 3.38, 3.53, 3.75]
s = len(Y)
step = abs(interval[0] - interval[1]) / (s)
X = np.arange(interval[0] + step / 2, interval[1], step)

#set up window
window = Tk()
window.geometry("500x725")
window.title("Lab2")

#set up plot
fig = Figure(figsize = (5, 5), dpi = 100)
subplot = fig.add_subplot(111)

def reset_view():
    global interval, subplot
    space = 2
    subplot.set_xlim(interval[0] - space, interval[1] + space)
    subplot.set_ylim(min(Y) - space, max(Y) + space)


#make it periodic in range from a to b
def bound(t, a, b):
    return a + (t - a) % (b - a) 

#Fourier coefficient Ak
def Ak(k):
    sum = 0.0
    for i in range(0, s):
        sum += Y[i] * np.cos(k * X[i])
    return (2 / s ) * sum

#Fourier coefficient Bk
def Bk(k):
    sum = 0.0
    for i in range(0, s):
        sum += Y[i] * np.sin(k * X[i])
    return (2 / s ) * sum

#Fourier series function F(x) with N members
def Furry(x, N):
    global A, B
    sum = A[0] / 2.0
    for n in range(1, N):
        sum += A[n] * np.cos(n * x) + B[n] * np.sin(n * x)
    return sum

#Fourier series in complex form
def FurryCompForm(x, N):
    global A, B
    sum = 0.0
    for k in range(N):
        sum += complex(A[k], -B[k]) / 2 * np.exp(complex(0, 1) * k * x)
    return sum.real

#method of least squares
def min_sqrt(matrix):
    height = len(matrix)
    width = len(matrix[0])
    normal = [[0.0] * width for _ in range(width - 1)]

    for k in range(width - 1):
        for i in range(height):
            for j in range(width):
                normal[k][j] += matrix[i][k] * matrix[i][j]

    return normal

#method of square root
def sqrt_root(matrix):
    height = len(matrix)
    width = len(matrix[0])

    if height != width - 1:
        return []

    matrix_to_det = [[matrix[i][j] for j in range(len(matrix[i]) - 1)] for i in range(len(matrix))]

    for i in range(height):
        for j in range(i):
            if matrix[i][j] != matrix[j][i]:
                return []

    det = np.linalg.det(matrix_to_det)

    if det < 0.0:
        return []

    L = np.zeros((height, height))
    for i in range(height):
        for j in range(i + 1):
            L[i][j] = matrix[i][j]
            for k in range(j):
                L[i][j] -= L[i][k] * L[j][k]
            if i == j:
                L[i][j] = np.sqrt(L[j][j])
            else:
                L[i][j] /= L[j][j]

    Y = np.zeros(height)
    for i in range(height):
        sum_val = 0.0
        for j in range(i):
            sum_val += L[i][j] * Y[j]
        Y[i] = (matrix[i][width - 1] - sum_val) / L[i][i]

    result = np.zeros(height)
    for i in range(height - 1, -1, -1):
        sum_val = 0.0
        for j in range(i, height):
            sum_val += L[j][i] * result[j]
        result[i] = (Y[i] - sum_val) / L[i][i]

    return result

#prepare matrix
matrix_min_sqrt = []
for i in range(s):
    matrix_min_sqrt.append([X[i] ** 2, X[i], 1, Y[i]])

#coefficients of second-order curve
ai = sqrt_root(min_sqrt(matrix_min_sqrt))


def plot(N):
    #calculate An and Bn once
    global A, B
    A = [Ak(n) for n in range(0, N)]
    B = [Bk(n) for n in range(0, N)]

    #define functions
    F = lambda x: Furry(x, N)
    Fc = lambda x: FurryCompForm(x, N)
    poly = lambda x: ai[0] * x * x + ai[1] * x + ai[2]

    #calculate errors
    def calcMeanAbsoluteError(func):
        error = 0.0
        for i in range(s):
            diff = abs(func(X[i]) - Y[i])
            error += diff
        return error / s
    
    def calcMeanSquaredError(func):
        error = 0.0
        for i in range(s):
            diff = abs(func(X[i]) - Y[i])
            error += diff * diff
        return error / s

    tableMatrix[1][1].config(text=f'{calcMeanAbsoluteError(F):.6f}')
    tableMatrix[2][1].config(text=f'{calcMeanAbsoluteError(poly):.6f}')
    tableMatrix[3][1].config(text=f'{calcMeanAbsoluteError(Fc):.6f}')
    tableMatrix[1][2].config(text=f'{calcMeanSquaredError(F):.6f}')
    tableMatrix[2][2].config(text=f'{calcMeanSquaredError(poly):.6f}')
    tableMatrix[3][2].config(text=f'{calcMeanSquaredError(Fc):.6f}')

    polynom_label.config(
        text = f"Polynom: {ai[0]:.6f}x^2 + {ai[1]:.6f}x + {ai[2]:.6f}")

    #draw
    subplot.clear()
    reset_view()
    subplot.plot(X, Y, 'o', label='discrete signal')
    subplot.plot(T, F(T), label='Fourier series')
    subplot.plot(T, poly(T), label='Second-order curve')
    subplot.plot(T, Fc(T), label='Fourier series(complex form)')
    subplot.legend()
    canvas.draw()


#validate entered n to be greater than 1 integer
def validate(inp):
    try:
        if not inp or int(inp) == 1:
            button.config(state = DISABLED)
            return True
        inp = int(inp)
        if inp > 1:
            button.config(state = NORMAL)
            return True
        else:
            button.config(state = DISABLED)
            return False
    except ValueError:
        return False


#set up ui
vcmd = window.register(validate)
header = Frame(window)
Label(header, text = "N = ").pack(side = LEFT)
entry = Entry(header, 
              width = 5, 
              validate = 'key', 
              validatecommand = (vcmd, '%P'))
entry.pack(side = LEFT)
button = Button(header, 
                text = "apply", 
                command = lambda: plot(int(entry.get())), 
                state = DISABLED)
button.pack(side = LEFT)
entry.insert(0, s)
polynom_label = Label(window, text = "")

#table preparing
table = Frame(master=window, borderwidth=1, relief='solid')
tableMatrix = []
for y in range(4):
    row = []
    for x in range(3):
        labelGrid = Label(master=table, text="", padx=4, pady=4)
        labelGrid.grid(row=y, column=x)
        row.append(labelGrid)
    tableMatrix.append(row)

tableMatrix[0][0].config(text = f'Approx\\Mean error')

tableMatrix[0][1].config(text = f'Absolute')
tableMatrix[0][2].config(text = f'Squared')

tableMatrix[1][0].config(text = f'Fourier series')
tableMatrix[2][0].config(text = f'Second-order curve')
tableMatrix[3][0].config(text = f'Fourier series (cf)')

#pack
header.pack(expand = 1)
table.pack()
polynom_label.pack()

#draw canvas
canvas = FigureCanvasTkAgg(fig, master = window)
canvas.draw()
canvas.get_tk_widget().pack()

#add toolbar
toolbar = NavigationToolbar2Tk(canvas, window)
toolbar.update()
canvas.get_tk_widget().pack()


window.mainloop()